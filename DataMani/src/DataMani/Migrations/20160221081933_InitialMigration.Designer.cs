using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using DataMani.Models;

namespace DataMani.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20160221081933_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DataMani.Models.Cameraman", b =>
                {
                    b.Property<int>("cameraManID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Achievements");

                    b.Property<DateTime>("DateOfBirth");

                    b.Property<string>("EducationQualification");

                    b.Property<string>("FirstName")
                        .IsRequired();

                    b.Property<string>("Language");

                    b.Property<string>("LastName");

                    b.Property<int>("LocationID");

                    b.Property<string>("Place");

                    b.Property<string>("Remuneration");

                    b.HasKey("cameraManID");
                });

            modelBuilder.Entity("DataMani.Models.Location", b =>
                {
                    b.Property<int>("LocationID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("DataMani.Models.Cameraman", b =>
                {
                    b.HasOne("DataMani.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationID");
                });
        }
    }
}
