﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DataMani.Models;
using System.IO;

namespace DataMani.Models
{
    public class Cameraman
    {

        [ScaffoldColumn(false)]
        [Key]
        public int cameraManID { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

       

        public string Place { get; set; }

        public string Language { get; set; }

        [Display(Name = "Expected Remuneration")]
        public string Remuneration { get; set; }

        public string Achievements { get; set; }

        [Display(Name = "Education Qualifications")]
        public string EducationQualification { get; set; }

        [ScaffoldColumn(true)]
        public int? LocationID { get; set; }

        public virtual Location Location { get; set; }


        // a list of all places the movie was filmed

        public List<Location> filmSets { get; set; }

        public static List<Cameraman> ReadAllFromCSV(string filepath)
        {
            List<Cameraman> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Cameraman.OneFromCsv(v))
                                        .ToList();
            return lst;
        }

        public static Cameraman OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Cameraman item = new Cameraman();

            int i = 0;
            item.LastName = Convert.ToString(values[i++]);
            item.FirstName = Convert.ToString(values[i++]);
            item.Place = Convert.ToString(values[i++]);
            item.Language = Convert.ToString(values[i++]);
            item.Remuneration = Convert.ToString(values[i++]);
            item.Achievements = Convert.ToString(values[i++]);
            item.EducationQualification = Convert.ToString(values[i++]);
            item.LocationID = Convert.ToInt32(values[i++]);

            return item;
        }

    }
}
