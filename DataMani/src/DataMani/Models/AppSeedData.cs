﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace DataMani.Models
{
    public class AppSeedData
    {

        public static void Initialize(IServiceProvider serviceProvider, string appPath)
        {
            string relPath = appPath + "//Models//SeedData//";
            var context = serviceProvider.GetService<AppDbContext>();

            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }

            context.Cameramans.RemoveRange(context.Cameramans);
            context.Locations.RemoveRange(context.Locations);
            context.SaveChanges();

            SeedLocationsFromCsv(relPath, context);
            SeedMoviesFromCsv(relPath, context);

            //     var l1 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
            //   var l2 = new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" };
            //    var l3 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
            //    var l4 = new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" };
            //    var l5 = new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India" };
            //  var l6 = new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India" };
            //  context.Locations.AddRange(l1, l2, l3, l4, l5, l6);
            //  context.Cameramans.AddRange(
            //     new Cameraman() { cameraManID = 003421, LastName = "Raja", FirstName = "Mouli", DateOfBirth = new DateTime(1984, 02, 23), Place = "Hyderabad", Language = "Telugu", Remuneration = "5 crores", Achievements = "Nandi Award Winner-1999", EducationQualification = "Arts degree" , LocationID = l1.LocationID },
            //        new Cameraman() { cameraManID = 02234, LastName = "Dhoni", FirstName = "Mahendra ", DateOfBirth = new DateTime(1978, 07, 29), Place = "delhi", Language = "Telugu", Remuneration = "5 crores", Achievements = "Nandi Award Winner-1999", EducationQualification = "Arts degree", LocationID = l2.LocationID },
            //      new Cameraman() { cameraManID = 002441, LastName = "virat", FirstName = "singh", DateOfBirth = new DateTime(1979, 12, 24), Place = "Hyderabad", Language = "Telugu", Remuneration = "5 crores", Achievements = "Nandi Award Winner-1999", EducationQualification = "Arts degree", LocationID = l3.LocationID },
            //     new Cameraman() { cameraManID = 003651, LastName = "Prabhas", FirstName = "rana", DateOfBirth = new DateTime(1968, 04, 17), Place = "Hyderabad", Language = "Telugu", Remuneration = "5 crores", Achievements = "Nandi Award Winner-1999", EducationQualification = "Arts degree", LocationID = l4.LocationID },
            //      new Cameraman() { cameraManID = 04601, LastName = "kohli", FirstName = "Mouli", DateOfBirth = new DateTime(1987, 06, 25), Place = "Hyderabad", Language = "Telugu", Remuneration = "5 crores", Achievements = "Nandi Award Winner-1999", EducationQualification = "Arts degree", LocationID = l5.LocationID }
            //      );
        }

        private static void SeedMoviesFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "cameraman.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            Cameraman.ReadAllFromCSV(source);
            List<Cameraman> lst = Cameraman.ReadAllFromCSV(source);
            context.Cameramans.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedLocationsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "location.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
            context.Locations.AddRange(lst.ToArray());
            context.SaveChanges();
        }

